#!/usr/bin/env python3

import requests
import time

# Target Var
target = input("Input the targeted url: ")

# Record the start time
start_time = time.time()

# Check if provided url has http or https attached, if not, add it
if not (target.startswith('http://') or target.startswith('https://')):
    target = 'https://' + target

# Load wordlist from file
with open("medium.txt", "r", errors='replace') as file:
    wordlist = file.read().splitlines()

def fuzz():
    total_words = len(wordlist)
    for i, word in enumerate(wordlist):
        response = requests.get(url=f"{target}/{word}")
        elapsed_time = time.time() - start_time
        if response.status_code != 404:  # Only proceed if the status code is not 404
            try:
                content_type = response.headers['Content-Type'].lower()
                if 'application/json' in content_type:
                    data = response.json()
                    print('{:<20} {:<10} {:<20} {:<10} {:<15} {}'.format("Word: " + word, "Status code: " + str(response.status_code), "Elapsed time: " + str(round(elapsed_time, 2)) + " seconds", "URL: " + response.url, "METHOD: GET", "Data: " + str(data)))
                elif 'text' in content_type:
                    data = response.text
                    if 'doctype html' in data.lower():
                        with open('output.html', 'a') as file:
                            file.write(data)
                    else:
                        print('{:<20} {:<10} {:<20} {:<10} {:<15} {}'.format("Word: " + word, "Status code: " + str(response.status_code), "Elapsed time: " + str(round(elapsed_time, 2)) + " seconds", "URL: " + response.url, "METHOD: GET", "Data: " + str(data)))
                else:
                    print('Unknown content type:', content_type)
            except ValueError:
                print("Unable to parse response.")
        # Display the progress after every word processed, regardless of its status code
        progress = (i + 1) / total_words * 100
        print(f'Processed: {progress:.2f}%')



fuzz()
